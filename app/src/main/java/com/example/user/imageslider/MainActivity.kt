package com.example.user.imageslider

import android.os.Bundle
import android.os.Handler
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {
    private var qustionModelArrayList: ArrayList<QuestionModel>? = null

    private val myQuestionList = arrayListOf<String>(
        "q1",
        "q2",
        "q3",
        "q4"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        qustionModelArrayList = ArrayList()
        qustionModelArrayList = populateList()

        init()

    }

    private fun populateList(): ArrayList<QuestionModel> {

        val list = ArrayList<QuestionModel>()

        for (question in myQuestionList) {
            val questionModel = QuestionModel(question)
            list.add(questionModel)
        }
        return list
    }

    private fun init() {

        mViewPager.adapter = QuestionsSlidingAdapter(this@MainActivity, this.qustionModelArrayList!!)

        mIndicator.setViewPager(mViewPager)
        val density = resources.displayMetrics.density

        mIndicator.setRadius(5 * density)
        var NUM_PAGES = qustionModelArrayList!!.size
        var currentPage: Int = 0
        // Auto start of viewpager
        val handler = Handler()
        val Update = Runnable {
            if (currentPage == NUM_PAGES) {
                currentPage = 0
            }
            mViewPager!!.setCurrentItem(currentPage++, true)
        }
        /*val swipeTimer = Timer()
        swipeTimer.schedule(object : TimerTask() {
            override fun run() {
                handler.post(Update)
            }
        }, 5000, 5000)*/

        // Pager listener over indicator
        mIndicator.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageSelected(position: Int) {
                currentPage = position

            }

            override fun onPageScrolled(pos: Int, arg1: Float, arg2: Int) {

            }

            override fun onPageScrollStateChanged(pos: Int) {
            }
        })

    }
}

